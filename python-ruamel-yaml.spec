%global _empty_manifest_terminate_build 0
%global pypi_name ruamel.yaml
Name:           python-ruamel-yaml
Version:        0.18.6
Release:        2
Summary:        ruamel.yaml is a YAML parser/emitter that supports roundtrip preservation of comments, seq/map flow style, and map key order
License:        MIT
URL:            https://sourceforge.net/projects/ruamel-yaml
Source0:        https://files.pythonhosted.org/packages/29/81/4dfc17eb6ebb1aac314a3eb863c1325b907863a1b8b1382cdffcb6ac0ed9/ruamel.yaml-0.18.6.tar.gz
BuildArch:      noarch
Patch0001:      0000-fix-big-endian-issues.patch
%description
ruamel.yaml ruamel.yaml is a YAML 1.2 loader/dumper package for Python.

%package -n python3-ruamel-yaml
Summary:        ruamel.yaml is a YAML parser/emitter that supports roundtrip preservation of comments, seq/map flow style, and map key order
Provides:       python-ruamel-yaml = %{version}-%{release}
Provides:       python%{python3_pkgversion}dist(ruamel-yaml) = %{version}
Provides:       python%{python3_version}dist(ruamel-yaml) = %{version}
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-setuptools_scm
BuildRequires:  python3-pytest
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-ruamel-yaml-clib

%description -n python3-ruamel-yaml
ruamel.yaml ruamel.yaml is a YAML 1.2 loader/dumper package for Python.

%package help
Summary:        ruamel.yaml is a YAML parser/emitter that supports roundtrip preservation of comments, seq/map flow style, and map key order
Provides:       python3-ruamel-yaml-doc
%description help
ruamel.yaml ruamel.yaml is a YAML 1.2 loader/dumper package for Python.

%prep
%autosetup -n ruamel.yaml-%{version} -p1

%build
%pyproject_build

%install
export RUAMEL_NO_PIP_INSTALL_CHECK=1
%pyproject_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/doclist.lst .

##Tests are not included in the upstream tarball
#%check
#PYTHONPATH=$(echo build/lib) py.test-%{python3_version} _test/test_*.py

%files -n python3-ruamel-yaml
%license LICENSE
%doc CHANGES README.md
%{python3_sitelib}/ruamel/yaml
%{python3_sitelib}/ruamel.yaml-*.dist-info/

%files help
%defattr(-,root,root)
%doc README.md CHANGES

%changelog
* Thu Mar 13 2025 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 0.18.6-2
- Fix python3dist(ruamel-yaml) missing

* Fri Mar 15 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 0.18.6-1
- Update package to version 0.18.6
- Fix issue with multiple document intermixing YAML 1.2 and YAML 1.1, the VersionedResolver now resets

* Fri Jul 14 2023 chenzixuan <chenzixuan@kylinos.cn> - 0.17.32-1
- Upgrade python3-ruamel-yaml to version 0.17.32

* Sat Jun 04 2022 OpenStack_SIG <openstack@openeuler.org> - 0.17.21-1
- Upgrade python3-ruamel-yaml to version 0.17.21

* Fri May 20 2022 yaoxin <yaoxin30@h-partners.com> - 0.17.20-1
- Update to 0.17.20

*Tue Mar 29 2022 xu_ping <xuping33@huawei.com> - 0.16.5-2
- Fix testcase test_anchor and test_deprecation error

* Mon Mar 2 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.16.5-1
- Package init
